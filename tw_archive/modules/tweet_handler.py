import urllib.request
import tw_archive.folders as folders
import os


def handle(tweet):
    result = []
    result.append(tweet["id_str"])
    result.append(tweet["created_at"])

    # Strip HTML tag
    result.append(
        tweet["source"][tweet["source"].index(">") + 1:-4])

    content = tweet["full_text"]

    # Replace t.co shortlinks with full links
    urls = tweet["entities"]["urls"]
    for url in urls:
        content = content.replace(url["url"], url["expanded_url"])

    # Download media files
    media_count = 0
    media_suffix = "i"  # i for images, v for videos, g for gifs
    if ("extended_entities" in tweet and "media" in tweet["extended_entities"]):
        print(f"Downloading media for tweet: {result[0]}")
        media_files = tweet["extended_entities"]["media"]
        for media in media_files:
            url = ""
            ending = "jpg"

            if (media["type"] == "photo"):
                url = media["media_url_https"]
            else:
                ending = "mp4"
                media_suffix = "g" if media["type"] == "animated_gif" else "v"
                variants = media["video_info"]["variants"]
                variants = sorted(
                    variants, key=lambda x: int(x.get("bitrate", "0")), reverse=True)
                url = variants[0]["url"]

            if url != "":
                path = os.path.join(folders.get_media_folder(
                ), f"{result[0]}_{media_count}.{ending}")
                try:  # most probably just catches 404 errors
                    urllib.request.urlretrieve(url, path)
                    media_count += 1
                except:
                    pass

            # Remove image URL from tweet text
            content = content.replace(media["url"], "")
    result.append(content.strip())
    result.append(f"{str(media_count)}{media_suffix}")
    return result
