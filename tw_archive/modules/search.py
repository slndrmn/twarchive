import csv
import getopt
import os
import pprint
import re
import sys
import time
import urllib.parse
import html
from datetime import datetime, timedelta
from optparse import OptionGroup
import tw_archive.folders as folders


def get_options(parser):
    parser.set_usage("twarchive search [options] <query>")

    opts = OptionGroup(parser, "Search options")
    opts.add_option("-a", "--archive", dest="archive_name", default="tweets",
                    help="The name of the archive file. Default: 'tweets'")
    opts.add_option("-t", "--type", dest="type", help="The type of the query. Accepted values are: text, client, date. Default is 'text'.",
                    choices=["text", "client", "date"], default="text")
    opts.add_option("-d", "--descending", default=False, action="store_true", dest="order_descending",
                    help="If the tweets should be ordered in descending order. Default is false.")
    opts.add_option("-i", "--case-sensitive", action="store_true", default=False, dest="case_sensitive",
                    help="The default value for the search case sensitivity. Default is false.")
    return opts


LINK_REGEX = re.compile(
    r"https?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")


def search(archive, query, query_type, case_sensitive, descending):
    """ Searches for tweets matching the given criteria.
        :param archive: the archive containing the tweets to search for.
        :param source: the name of the used client (e.g. Twitter for iPhone).
        :param text: textual content of the tweet.
        :param date: ...
        :param case_sensitive: whether or not the search should be case sensitive. Default: False
        :returns: a list of matching tweets, where each tweet is a list:
        [id, datetime, source, content]. """
    tweet_file = csv.reader(
        open(folders.get_archive_file(archive), "r", encoding="utf-8"))
    result_set = []

    regex = re.compile(query, re.IGNORECASE | re.MULTILINE | re.DOTALL)
    if (case_sensitive):
        regex = re.compile(query, re.DOTALL | re.MULTILINE)

    for tweet in tweet_file:
        _tweet = tweet[:]

        # skip header
        if tweet[0] == "id":
            continue

        _tweet[1] = pretty_date(tweet[1])
        _tweet.append(timestamp(tweet[1]))

        _tweet[4] = int(tweet[4][0])  # Convert media count to int
        _tweet.append(tweet[4][1])  # append media suffix

        append = True
        if query_type == "client":
            append = regex.search(tweet[2]) != None

        if query_type == "date":
            append = regex.search(parseDate(tweet[1])) != None

        if query_type == "text" and append:
            content = urllib.parse.unquote(LINK_REGEX.sub("", tweet[3]))
            content = html.unescape(content)

            append = regex.search(content) != None

        if append:
            result_set.append(_tweet)

    return sorted(result_set, key=lambda tw: tw[5], reverse=descending)


def parseDate(date):
    """Converts a Twitter datetime string into the YYYY-mm-dd standard."""
    new_date = datetime.strptime(date, "%a %b %d %X %z %Y")
    result = str(new_date.strftime("%Y-%m-%d"))
    return result


def pretty_date(date):
    """ Prettifies a date given in the default Twitter API format.
        :param date: the date, as string
        :returns: the date, as string, but pretty """
    new_date = datetime.strptime(date, "%a %b %d %X %z %Y")
    new_date += timedelta(seconds=time.localtime().tm_gmtoff)
    return str(new_date.strftime("%b %d, %Y at %X"))


def timestamp(date):
    """Gets the timestamp of a Twitter API string
       :param date: The date as string
       :returns: The POSIX timestamp """
    date = datetime.strptime(date, "%a %b %d %X %z %Y")
    return date.timestamp()
