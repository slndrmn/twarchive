import csv
import json
import os
import sys
from twython import Twython
from optparse import OptionGroup
import tw_archive.folders as folders
import tw_archive.modules.tweet_handler as tw


def get_options(parser):
    opts = OptionGroup(parser, "Fetch options")
    opts.add_option("-u", "--username",
                    action="store",
                    dest="username",
                    default=None,
                    help="The username of the account which should be fetched. Optional.")
    opts.add_option(
        "--archive", dest="archive_name", default="tweets", help="The name of the archive. Default is: 'tweets'")
    opts.add_option(
        "-c", "--count", dest="fetch", type="int", default=200, help="The number of tweets that are fetched, maximum is 200. Default is 200.")
    opts.add_option("--retweets", action="store_true", dest="include_retweets", default=False,
                    help="Whether retweets should be included during fetching or not. Default is false.")

    return opts


def fetch(username, archive_name, count, include_retweets):
    """ Looks for new tweets.
        :returns: Amount of new tweets added to the archive. """

    CSV_FILE_NAME = folders.get_archive_file(archive_name)

    # touch file and add csv header
    if not os.path.exists(CSV_FILE_NAME):
        f = open(CSV_FILE_NAME, "w", encoding="utf-8")
        f.write("id,created_at,source,text\n")
        f.close()

    tweet_file = csv.writer(
        open(CSV_FILE_NAME, "a", encoding="utf-8", newline=""))

    # open key file or exit if file does not exist
    try:
        key_file = open(folders.get_keys_file(), "r")
    except FileNotFoundError:
        exit("[ERROR] Missing keys.json file.")

    keys = json.loads(key_file.read())

    # exit if keys are incomplete
    if len(keys) != 4:
        print("[ERROR] Invalid key file. Make sure that all 4 needed keys are included and seperated by line breaks.")
        exit(1)

    # check if each key is at least 20 chars long
    for _, value in keys.items():
        if len(value) < 20:
            exit(
                "[ERROR] Your key file contains at least one invalid key. Please reconfigure it.")

    # map keys and create Twitter client
    CONSUMER_KEY = keys["CONSUMER_KEY"]
    CONSUMER_SECRET = keys["CONSUMER_SECRET"]
    ACCESS_KEY = keys["ACCESS_KEY"]
    ACCESS_SECRET = keys["ACCESS_SECRET"]

    # create Twitter API client
    twitter = Twython(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_KEY, ACCESS_SECRET)

    file = open(CSV_FILE_NAME, "r", encoding="utf-8")
    reader = csv.reader(file)
    lines = []
    for row in reader:
        lines.append(row)
    tail = lines[-1]
    offset = int(tail[0]) if tail[0] != "id" else 1
    file.close()

    # fetch the newest (up to 200) tweets that are newer than the newest
    # archived tweet
    result_set = twitter.get_user_timeline(count=count, since_id=offset,
                                           include_rts=str(include_retweets).lower(), trim_user="false", tweet_mode="extended",
                                           screen_name=(username if username else None))
    result_set = list(reversed(result_set))  # from oldest to newest
    tweets = []
    for tweet in result_set:
        new_tweet = tw.handle(tweet)
        tweets.append(new_tweet)

    tweet_file.writerows(tweets)

    return len(tweets)
