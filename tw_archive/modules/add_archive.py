import json
import csv
import getopt
import os
import sys
from datetime import datetime
from optparse import OptionGroup
import tw_archive.folders as folders
import tw_archive.modules.tweet_handler as tw


def get_options(parser):
    opts = OptionGroup(parser, "Archive options")
    opts.add_option("-i", "--input", dest="in_archive", default="tweet.js",
                    help="The input file in the JS format by Twitter. Usally the tweet.js. Default is 'tweet.js'.")
    opts.add_option("-o", "--output", dest="out_archive",
                    default="tweets", help="The output file. Default is 'tweets'")
    opts.add_option("--retweets", action="store_true", dest="include_retweets", default=False,
                    help="Whether retweets should be included during fetching or not. Default is false.")

    return opts


def add(in_archive, out_archive, include_retweets):
    INPUT_FILE_NAME = os.path.abspath(in_archive)
    OUTPUT_FILE_NAME = folders.get_archive_file(out_archive)

    if not os.path.exists(OUTPUT_FILE_NAME):
        print("[INFO] The specified CSV output file doesn't exist. Creating new one.")
        f = open(OUTPUT_FILE_NAME, "w", encoding="utf-8")
        f.close()

    # Check if input file is properly formatted
    with open(INPUT_FILE_NAME) as f:
        lines = f.readlines()

    if lines[0][0:6] == "window":
        # Replace first line
        lines[0] = "[{"

    with open(INPUT_FILE_NAME, "w") as f:
        f.writelines(lines)

    # Read tweets into CSV
    tweets = []
    with open(INPUT_FILE_NAME, "r", encoding="utf-8") as f:
        d = json.load(f)
        print("Number of tweets in the archive:", len(d))

        for tweet in d:
            # skip retweets
            if not include_retweets and tweet["full_text"][:4] == "RT @":
                continue

            new_tweet = tw.handle(tweet)
            tweets.append(new_tweet)

    output_file = csv.writer(open(OUTPUT_FILE_NAME, "w", encoding="utf-8"))
    output_file.writerow(["id", "created_at", "source", "text", "media_count"])
    output_file.writerows(list(reversed(tweets)))
