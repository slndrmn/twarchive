from optparse import OptionGroup
from tw_archive.webapp import app


def get_options(parser):
    opts = OptionGroup(parser, "Web options")
    opts.add_option("--port", type="int", dest="port",
                    default=5000, help="The port of the web interface. Default is 5000")
    opts.add_option("--host", default="127.0.0.1", dest="host",
                    help="The host of the web interface. Default is 127.0.0.1")
    opts.add_option("-d", "--descending", default=False, dest="order_descending", action="store_true",
                    help="If the tweets should be ordered in descending order. Default is false.")
    opts.add_option("-c", "--case-sensitive", default=False, dest="case_sensitive", action="store_true",
                    help="The default value for the search case sensitivity. Default is false.")
    opts.add_option("-a", "--archive", dest="archive_name", default="tweets",
                    help="The name of the archive file. Default: 'tweets'")
    return opts


def run(host, port, debug, case_sensitive, descending, archive_name):
    app.config["case_sensitive"] = case_sensitive
    app.config["archive_name"] = archive_name
    app.config["order_descending"] = descending
    app.run(host=host, port=port, debug=debug)
