from tw_archive.webapp.forms import QueryForm
from tw_archive.modules import fetch as fetch_tweets, search
from flask import Flask, redirect, render_template, request, url_for, send_from_directory
import os

app = Flask(__name__)

# for CSRF tokens
app.config["SECRET_KEY"] = "5669d9237210eabb2ea70a50243eebb4"

@app.route("/", methods=["GET", "POST"])
def home():
    case_sensitive = app.config["case_sensitive"]
    order_descending = app.config["order_descending"]
    archive_name = app.config["archive_name"]

    form = QueryForm()
    if form.validate_on_submit():
        # check the optional query parameters
        case_sensitive = request.values.get(
            "case_sensitive", case_sensitive)
        order_descending = request.values.get(
            "descending", order_descending)
        query_type = request.values.get("rf")
        query = request.values.get("query")
        archive = request.values.get(
            "archive_name", archive_name)

        if (case_sensitive is not None):
            case_sensitive = bool(case_sensitive)
        if (order_descending is not None):
            order_descending = bool(order_descending)

        if (archive == ""):
            # Using the default value itself doesn't work, as there's always a value submitted.
            # In most cases this value is therefore empty.
            archive = archive_name

        # Search for tweets
        result_set = search.search(
            archive=archive,
            query=query,
            query_type=query_type,
            case_sensitive=case_sensitive,
            descending=order_descending
        )

        return render_template(
            "results.html",
            results=result_set,
            query=query,
            type=query_type,
            title="Results",
            count=len(result_set)
        )
    else:
        return render_template("results.html", form=form, title="Search")

@app.route('/media/<path:path>')
def get_media_file(path):
    video_path = os.path.join( os.path.dirname(__file__), "../../data/media", path + ".mp4")
    if (os.path.isfile(video_path)):
        return send_from_directory('../../data/media', path + ".mp4")
    return send_from_directory('../../data/media', path + ".jpg")
