(() => {
    const placeholders = [
        "The content phrase", // text
        "The name of the used client (e.g. Twitter for iPhone)", // source
        "The date of the tweet, given as a substring"  // date
    ];
    const queryElement = document.getElementById("query");

    for (let i = 0; i < 3; i++) {
        const elem = document.getElementById("rf-" + i);
        elem.addEventListener("click", () => {
            // Set placeholder of input field
            queryElement.placeholder = placeholders[i];

            // Disable checkbox if queryElement is not for text
            document.getElementById("case_sensitive").disabled = i != 0;

            // date time
            queryElement.type = i == 2 ? "date" : "text";
        });
    }

    // Select "Text" RadioButton as the default search option
    document.getElementById("rf-0").checked = "checked";
})();
