import os
ROOT = os.path.dirname(os.path.dirname(__file__))
CONFIG_ROOT = os.path.join(ROOT, "config")
DATA_ROOT = os.path.join(ROOT, "data")


def createFolders():
    if not (os.path.exists(DATA_ROOT)):
        os.mkdir(DATA_ROOT)

    if not (os.path.exists(CONFIG_ROOT)):
        os.mkdir(CONFIG_ROOT)


def get_keys_file():
    createFolders()
    return os.path.join(CONFIG_ROOT, "keys.json")


def get_archive_file(name='tweets'):
    createFolders()
    return os.path.join(DATA_ROOT, name + ".csv")

def get_media_folder():
    createFolders()
    media_root = os.path.join(DATA_ROOT, "media")
    if not (os.path.exists(media_root)):
        os.mkdir(media_root)

    return media_root
