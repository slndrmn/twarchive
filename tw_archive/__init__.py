from optparse import OptionParser, OptionGroup
import sys
import pprint
from tw_archive.modules import fetch, web, search, add_archive

allowed_methods = ["fetch", "web", "search", "add"]


def generate_configuration(method):
    parser = OptionParser(usage=f"twarchive <method> [options]\t(methods: {', '.join(allowed_methods)})")
    debug_options_group = OptionGroup(parser, "Debug options")
    debug_options_group.add_option("--debug", action="store_true",
                                   default=False, help="Turns the debug mode on.")

    if (method == "web"):
        parser.add_option_group(web.get_options(parser))

    if (method == "fetch"):
        parser.add_option_group(fetch.get_options(parser))

    if (method == "search"):
        parser.add_option_group(search.get_options(parser))

    if (method == "add"):
        parser.add_option_group(add_archive.get_options(parser))

    parser.add_option_group(debug_options_group)
    return parser


def main():
    if sys.argv[1:]:
        cmd = sys.argv[1]
        rest = sys.argv[2:]
        parser = generate_configuration(cmd)
        (options, args) = parser.parse_args(rest)
    else:
        parser = generate_configuration("[command]")
        parser.print_usage()
        exit(1)

    if (cmd not in allowed_methods):
        parser.print_usage()
        exit(1)

    if (cmd == "fetch"):
        number_of_tweets = fetch.fetch(
            options.username, options.archive_name, options.fetch, options.include_retweets)
        print(f" > {number_of_tweets} new tweets fetched.")
        exit(0)

    if (cmd == "web"):
        web.run(options.host, options.port, options.debug, options.case_sensitive, options.order_descending, options.archive_name)
        exit(0)

    if (cmd == "search"):
        if (len(args) == 0):
            parser.error("You need to provide a search query.")
            exit(1)

        results = search.search(options.archive_name, args[0], options.type,
                                options.case_sensitive, options.order_descending)
        pprint.pprint(results)
        exit(0)

    if (cmd == "add"):
        add_archive.add(options.in_archive, options.out_archive,
                        options.include_retweets)
        exit(0)
