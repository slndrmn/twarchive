install:
	pip3 install --user --requirement requirements.txt

start:
	./twarchive web

fetch:
	./twarchive fetch

ustart:
	git pull && make start
