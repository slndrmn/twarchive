# TwArchive

## Description

**TwArchive** is an application written in Python that makes archiving and searching through your tweets damn easy. It allows to:

- import an existing archive
- fetch new tweets from your timeline
- search through the archive (by text, date or client)

This application in special is very handy with the new archive format from Twitter that just throws a bunch of `.js` files on you.

## Requirements

* Python 3.6+

## Installation

```bash
make install
```

Alternatively, simply install the pip requirements:
```bash
pip3 install -r requirements.txt [--user]
```

## Usage
```
Usage: ./twarchive <method> [options]     (methods: fetch, web, search, add)
```

Methods:
- `fetch`: Fetches the newest tweets.
- `web`: Starts the internal web server.
- `search`: Searches the tweets locally on the command line.
- `add`: Adds an archive file.

### Create your Twitter archive

1. When you request your Twitter data, Twitter will provide you a zip-compressed archive. This archive contains a file called `tweet.js`. Extract it and put it into the main directory of TwArchive.
2. Run `./twarchive add`. It'll create a new `tweets.csv` file in the `data` directory. This file can be seen as a "database", that holds all your tweets in a readable format.
3. `./twarchive fetch` is meant to run periodically, for example by using a cronjob. It will fetch the last 200 tweets (or less) and add them to the `tweets.csv` file. As a result, your Twitter archive will always be up-to-date.

For using `./twarchive fetch`, you need an app for the Twitter Developer API which you can [create here](https://apps.twitter.com). The app needs read and write access (access to direct messages is not necessary). Afterwards, you will have four keys which you need to put into a file called `keys.json` in the `config` folder. Take a look at `keys.json.template` for an example (or simply edit the file and rename it).

### Run TwArchive

TwArchive is meant to run locally as a web application on port `5000`. Use `make start` or run `./twarchive web` to launch the app.

## Configuration
The configuration is done using the command line. By using the `--help` parameter, the detailed description of options can be seen.
